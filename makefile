# Make DAQ tools
#
# PG v.0.1 18 June 2018
#-------------------------------------------------------
project = cmd_watch rd_rawfile replay_daq

#----------------------------------------------------- check shell variables
define wrn_
$(info >> Warning: undefined shell variable $(1) (used in $(project) $(2))<<)
endef 
define show_
$(info OK: $(1)=$($(1)))
endef

ifndef BASE_CH
$(info Error: shell variable BASE_CH is not defined)
$(info Read the makefile header info for details)
$(error )
else
$(call show_,BASE_CH)
endif

ifndef CONTROL_HOST
$(call wrn_,CONTROL_HOST,as default dispatcher host)
else 
$(call show_,CONTROL_HOST)
endif
#----------------------

CC=gcc

CFLAGS += -Wall  -O $(EXTRA_FLAG) -I.  -I$(BASE_CH)/include
LDLIBS += -lm -lpthread -L$(BASE_CH)/lib -lconthost  

all : $(project) 

clean :
	rm -f *.o *~ $(project)
help:
	@echo 'make            build most recent source' 
	@echo 'make -Wxx.c     force re-building xx'                     
	@echo 'make -B         force re-building everything'                     
	@echo 'make clean      remove all binaries and garbage'                             
	@echo 'make help       show this help'                                  

.PHONY: all clean help 


