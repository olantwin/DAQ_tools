## Utility programs for debugging and testing the SHiP Test Beam DAQ s/w.

Currently included:

  - **cmd_watch**: 
    to monitor DAQ ControlHost traffic and, optionally, write the received
    data on a local disk

  - **rd_rawfile**:
    to read a rawdata file recorded in the standard format (e.g., the
    one created by cmd_watch with the recodring option) and produce a 
    file summary

  - **replay_daq**:
    to read rawdata file and "replay" it by sending selected frames to
    Dispatcher. Optionally, the RC commands can be simulated.

  - **doc/**:
    Brief descriptions of the included programs
 